import cv2
import pickle
import numpy as np
import matplotlib.pyplot as plt

# 加载保存的模型clf
with open(r"E:\模式识别\clf.pkl", "rb") as file:
    clf = pickle.load(file)

# 读取原图和分割图
original_image = cv2.imread(r"C:\Users\ASUS\Desktop\data\Sandstone_2.tif", 0)
segment = cv2.imread(r"C:\Users\ASUS\Desktop\data\Sandstone_2_segment.tif", 0)

# 添加额外的特征 - 均值滤波
mean_filtered = cv2.blur(original_image, (5, 5))

# 将原图和额外的特征合并为新的特征矩阵
features = np.concatenate((original_image.reshape(-1, 1), mean_filtered.reshape(-1, 1)), axis=1)

# 对新的特征矩阵进行预测
predicted_segment = clf.predict(features[:, [0]])  # 仅使用原始图像特征进行预测
predicted_segment = predicted_segment.reshape(original_image.shape)

# 计算准确率
accuracy = (predicted_segment == segment).mean()
print("准确率:", accuracy)

# 显示原图、原分割图和推理分割图
fig, axes = plt.subplots(1, 3, figsize=(12, 4))

axes[0].imshow(original_image, cmap='gray')
axes[0].set_title("Original Image")

axes[1].imshow(segment, cmap='gray')
axes[1].set_title("Segment")

axes[2].imshow(predicted_segment, cmap='gray')
axes[2].set_title(f"Segmentation (acc: {accuracy:.3f})")

for ax in axes:
    ax.axis('off')

plt.tight_layout()
plt.show()